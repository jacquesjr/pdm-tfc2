package br.edu.iftm.pdm.pdmfood.ui.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.data.DAOStoresSingleton;
import br.edu.iftm.pdm.pdmfood.model.Pizza;

public class MainActivity extends AppCompatActivity {

    private static final int FORM_REQUEST_CODE = 1578;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickNewPizza(View view) {
        Intent openStoreForms = new Intent(this, NewPizzaActivity.class);
        startActivityForResult(openStoreForms, FORM_REQUEST_CODE);
    }

    public void onClickListPizzas(View view) {
        Intent listAllPizzas = new Intent(this, ListPizzasActivity.class);
        startActivity(listAllPizzas);
    }

    //---------------- HOOKS DA ACTIVITY

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == FORM_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Pizza pizza = data.getParcelableExtra(NewPizzaActivity.RESULT_KEY);
            DAOStoresSingleton.getINSTANCE().addPizza(pizza);
        }
    }
}