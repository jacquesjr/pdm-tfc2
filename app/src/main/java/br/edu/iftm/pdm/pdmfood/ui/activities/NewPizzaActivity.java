package br.edu.iftm.pdm.pdmfood.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.BreakIterator;
import java.text.DecimalFormat;

import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.model.Pizza;

public class NewPizzaActivity extends AppCompatActivity {

    public static final String RESULT_KEY = "NewPizzaActivity.RESULT_KEY";
    private TextView etxtPizzaName;
    private TextView numberPrice;
    private TextView etxtIngredients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        this.etxtPizzaName = findViewById(R.id.etxtPizzaName);
        this.numberPrice = findViewById(R.id.numberPrice);
        this.etxtIngredients = findViewById(R.id.etxtIngredients);
    }

    public void onClickSave(View view) {
        String name = this.etxtPizzaName.getText().toString();
        Float price =  Float.parseFloat(this.numberPrice.getText().toString());
        String ingredients = this.etxtIngredients.getText().toString();

        if(name.isEmpty() ||  price == null || ingredients.isEmpty()) {
            return;
        }

        Pizza pizza = new Pizza(name, new DecimalFormat("#,##0.00").format(price*0.25), new DecimalFormat("#,##0.00").format(price), new DecimalFormat("#,##0.00").format(price*1.25), ingredients);

        Intent output = new Intent();
        output.putExtra(RESULT_KEY, pizza);
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}