package br.edu.iftm.pdm.pdmfood.data;

import java.util.ArrayList;

import br.edu.iftm.pdm.pdmfood.model.Pizza;

public class DAOStoresSingleton {
    private static DAOStoresSingleton INSTANCE;
    private ArrayList<Pizza> pizzas;

    private DAOStoresSingleton() {
        this.pizzas = new ArrayList<>();
    }

    public static DAOStoresSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOStoresSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Pizza> getPizzas() {
        return this.pizzas;
    }

    public void addPizza(Pizza pizza) { this.pizzas.add(pizza); }
}
