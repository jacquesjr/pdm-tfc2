package br.edu.iftm.pdm.pdmfood.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza implements Parcelable {

    private String name;
    private String priceP;
    private String priceM;
    private String priceG;
    private String ingredients;

    public Pizza(String name, String priceP, String priceM, String priceG, String ingredients) {
        this.name = name;
        this.priceP = priceP;
        this.priceM = priceM;
        this.priceG = priceG;
        this.ingredients = ingredients;
    }

    public String getName() {
        return name;
    }

    public String getPriceP() {
        return priceP;
    }

    public String getPriceM() {
        return priceM;
    }

    public String getPriceG() {
        return priceG;
    }

    public String getIngredients() {
        return ingredients;
    }

    //------------ IMPLEMENTACAO DO PARCELABLE

    protected Pizza(Parcel in) {
        name = in.readString();
        priceP = in.readString();
        priceM = in.readString();
        priceG = in.readString();
        ingredients = in.readString();
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(priceP);
        dest.writeString(priceM);
        dest.writeString(priceG);
        dest.writeString(ingredients);
    }
}
