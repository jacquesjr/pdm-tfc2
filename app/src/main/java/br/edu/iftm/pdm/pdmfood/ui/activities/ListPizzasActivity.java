package br.edu.iftm.pdm.pdmfood.ui.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.iftm.pdm.pdmfood.R;
import br.edu.iftm.pdm.pdmfood.data.DAOStoresSingleton;
import br.edu.iftm.pdm.pdmfood.model.Pizza;

public class ListPizzasActivity extends AppCompatActivity {
    private LinearLayout lnlvListaPizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardapio);
        this.lnlvListaPizza = findViewById(R.id.rvPizzas);
        this.buildListPizza();
    }

    private void buildListPizza() {
        for (Pizza pizza : DAOStoresSingleton.getINSTANCE().getPizzas()) {
            View view = getLayoutInflater().inflate(R.layout.activity_cardapio, this.lnlvListaPizza, false);
            TextView txtValorLabel = view.findViewById(R.id.numberPrice);
            txtValorLabel.setText("R$" + String.valueOf(pizza.getPriceM()));
            TextView txtDescricaoLabel = view.findViewById(R.id.etxtIngredients);
            txtDescricaoLabel.setText(pizza.getIngredients());
            view.setTag(pizza);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Pizza p = (Pizza) v.getTag();
                    showPizzaInfo(p);
                }
            });
            this.lnlvListaPizza.addView(view);
        }


    }

    private void showPizzaInfo(Pizza pizza) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.app_name);
        View view = getLayoutInflater().inflate(R.layout.show_store_info_dialog, null);
        dialogBuilder.setView(view);
        TextView txtDescricaoPizza = view.findViewById(R.id.etxtIngredients);
        txtDescricaoPizza.setText(pizza.getIngredients());
        Button btnP = view.findViewById(R.id.numberPrice);
        btnP.setText("R$" + String.valueOf(pizza.getPriceP()));
        Button btnM = view.findViewById(R.id.numberPrice);
        btnM.setText("R$" + String.valueOf(pizza.getPriceM()));
        Button btnG = view.findViewById(R.id.numberPrice);
        btnG.setText("R$" + String.valueOf(pizza.getPriceG()));
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.create().show();
    }
}
